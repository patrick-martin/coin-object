const main_div = document.getElementById('main')
const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random() * 2)
        return this.state //should generate either 1 or 0
    },
    toString: function() {
        if (this.state === 1) {
            return 'Heads';
        } else {
            return 'Tails';
        }
    },
    toHTML: function() {
        const image = document.createElement('img');
        if (this.state === 1) {
            image.src = 'heads.jpg';
        } else {
            image.src = 'tails.jpeg';
        }
        main_div.appendChild(image)
        return image;
    }
};

function helperFunction(exercise, answer) {
    var newElement1 = document.createElement('div');
    var exerciseElement = document.createElement('p');
    var exercise1 = document.createTextNode(exercise);
    exerciseElement.appendChild(exercise1);
    newElement1.appendChild(exerciseElement);
    main_div.appendChild(newElement1);

    var newElement2 = document.createElement('div');
    var answerElement = document.createElement('p');
    var answer1 = document.createTextNode(answer);
    answerElement.appendChild(answer1);
    newElement2.appendChild(answerElement);
    main_div.appendChild(newElement2);
}
//SHOULD display array consisting of 20 flips (1 or 0)
function display20Flips() {
    const results = [];
    for (i = 0; i < 20; i++) {
        let coinFlip = coin.flip();
        results.push(coinFlip);
    }
    helperFunction('Display 20 Flips:', results)
    return results;
}
console.log(display20Flips())

//Displays 20 Images (Heads or Tails)
function display20Images() {
    const results = [];
    for (i = 0; i < 20; i++) {
        let coinFlip = coin.flip();
        results.push(coinFlip);
        coin.toHTML();
    }
    helperFunction('Display 20 Images:', results)
    return results;
}
//display20Images();
console.log(display20Images())